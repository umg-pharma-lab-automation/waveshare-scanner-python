Welcome to the ws-barcode-scanner documentation!
================================================

.. toctree::
   :maxdepth: 2

This module is an unofficial Python interface for the `Waveshare Barcode Scanner Module <https://www.waveshare.com/barcode-scanner-module.htm>`_.

Installation
------------

Run ``pip install ws-barcode-scanner`` to install the latest version.

Device setup
------------

There are two ways to configure the device:

1. By scanning the QR codes from the `manual <https://www.waveshare.com/w/upload/d/dd/Barcode_Scanner_Module_Setting_Manual_EN.pdf>`_
2. Via serial communication

To interact with the device, this library uses serial communication, which can be achieved via either the USB or the UART port.
To enable serial communication via USB, the device has to be configured to use the USB port as virtual serial port.

.. figure:: qr-codes.png
    :alt: QR codes for device configuration

    ..

    QR codes for device configuration (taken from the `manual <https://www.waveshare.com/w/upload/d/dd/Barcode_Scanner_Module_Setting_Manual_EN.pdf>`_)

Usage
-----

To control the scanner, use the :py:class:`BarcodeScanner <ws_barcode_scanner.BarcodeScanner>` class.

.. code-block::

    >>> from ws_barcode_scanner import BarcodeScanner

    >>> scanner = BarcodeScanner("COM3")

Barcode scanning
^^^^^^^^^^^^^^^^

When the device scans a code, it sends it to the device.
The method :py:func:`query_for_codes <ws_barcode_scanner.BarcodeScanner.query_for_codes>` reads and returns all codes that were scanned since the method was last called.
The properties :py:func:`last_code <ws_barcode_scanner.BarcodeScanner.last_code>` and :py:func:`last_timestamp <ws_barcode_scanner.BarcodeScanner.last_timestamp>` store the last code that was read, and the timestamp of that event.

Configuration
^^^^^^^^^^^^^

The method :py:func:`restore_factory_settings <ws_barcode_scanner.BarcodeScanner.restore_factory_settings>` resets all user settings.
:py:func:`save_to_flash <ws_barcode_scanner.BarcodeScanner.save_to_flash>` writes the current settings to the persistent flash memory (some settings would otherwise be lost when the device is restarted).

All other configuration has to be made via the :py:attr:`memory_map <ws_barcode_scanner.BarcodeScanner.memory_map>`.
It is an interface to the address table from the `manual <https://www.waveshare.com/w/upload/d/dd/Barcode_Scanner_Module_Setting_Manual_EN.pdf>`_ (pages 72-93).
Note that the manual refers to the highest bit as "7", while this library starts with "0":

.. code-block::

    >>> # read data
    >>> scanner.memory_map[0x0000, 0]  # first bit of the first byte
    True  # flash LED on scan
    >>> scanner.memory_map[0x0000, 1]
    True  # beep on scan
    >>> scanner.memory_map[0x0000, 2:4]
    1  # red target LED on during scanning
    >>> scanner.memory_map[0x0000, 4:6]
    1  # white LED on during scanning
    >>> scanner.memory_map[0x0000, 6:8]
    0  # manual scanning mode

    >>> # write data
    >>> scanner.memory_map[0x0000, 1] = False  # no beep on scan
    >>> scanner.memory_map[0x0000, 4:6] = 0b11  # keep white LED  always on

The BarcodeScanner class
------------------------

.. autoclass:: ws_barcode_scanner.BarcodeScanner

Memory Map
----------

.. autoclass:: ws_barcode_scanner.memory_map.MemoryMap
    :special-members: __getitem__, __setitem__
    :undoc-members: __getitem__, __setitem__
